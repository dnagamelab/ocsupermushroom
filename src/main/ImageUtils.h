#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface ImageUtils : NSObject
+ (NSArray *)maskFromImage:(UIImage *)_image withPointSize:(NSUInteger)_pointSize;

+ (NSArray *)pixelsFromImage:(UIImage *)_image atX:(NSUInteger)_x andY:(NSUInteger)_y count:(NSUInteger)_count;

@end



