#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

@class Option;


@interface MarkableGrid : NSObject {
    CGSize areaSize;
    NSUInteger pointSize;

    NSUInteger width;
    NSUInteger height;
    NSUInteger size;
    NSMutableArray *grid;
}
@property(nonatomic, readonly) NSUInteger width;
@property(nonatomic, readonly) NSUInteger height;


+ (id)gridWithArea:(CGSize)_areaSize fieldSize:(NSUInteger)_fieldSize;
- (id)initWithArea:(CGSize)_areaSize fieldSize:(NSUInteger)_fieldSize;

- (void)mark:(CGPoint)_point withValue:(id)_value;
- (void) unmark:(CGPoint)_point;


- (NSArray *)fields;


- (Option *)fieldAtPoint:(CGPoint)_point;
@end