#import "CapturingLineDrawer.h"


@implementation CapturingLineDrawer


+ (id)drawerWithArea:(CGRect)_area pointSize:(NSUInteger)_pointSize strokeSize:(CGFloat)_strokeSize {
    return [[CapturingLineDrawer alloc] initWithArea:_area pointSize:_pointSize strokeSize:_strokeSize];
}

- (id)initWithArea:(CGRect)_area pointSize:(NSUInteger)_pointSize strokeSize:(CGFloat)_strokeSize {
    if ((self = [super init])) {
        area = _area;
        mask = [MarkableGrid gridWithArea:_area.size fieldSize:_pointSize];

        drawer = [LineDrawer node];
        [drawer setDelegate:self];
        [drawer setStrokeSize:_strokeSize];

        [self addChild:drawer];
    }

    return self;

}

- (NSArray *)mask {
    return [mask fields];
}


- (void)lineDrawerPointAdded:(CGPoint)_point withSize:(CGFloat)_size {
    CGPoint touchInWorld = [self convertToWorldSpace:[self convertToNodeSpace:_point]];
    CGPoint topLeftCorder = ccpAdd(touchInWorld, ccp(-_size / 2, -_size / 2));
    for (int pointX = 0; pointX < _size; pointX++) {
        for (int pointY = 0; pointY < _size; pointY++) {
            [self updateMaskWithTouch:ccpAdd(topLeftCorder, ccp(pointX, pointY))];
        }
    }

    [delegate lineDrawerPointAdded:_point withSize:_size];
}

- (void)lineDrawerBegin:(CGPoint)_point withSize:(CGFloat)_size {
    [delegate lineDrawerBegin:_point withSize:_size];
}

- (void)lineDrawerEnd:(CGPoint)_point withSize:(CGFloat)_size {
    [delegate lineDrawerEnd:_point withSize:_size];
}

- (void)updateMaskWithTouch:(CGPoint)point {
    if ([self touchWithinArea:point]) {
        CGPoint localTouch = ccpSub(point, area.origin);
        localTouch = ccp(localTouch.x, area.size.height - localTouch.y);
        [mask mark:localTouch withValue:[NSNumber numberWithBool:YES]];
    }
}

- (BOOL)touchWithinArea:(CGPoint)touchInWorld {
    return touchInWorld.x > area.origin.x &&
            touchInWorld.x < area.origin.x + area.size.width &&
            touchInWorld.y > area.origin.y &&
            touchInWorld.y < area.origin.y + area.size.height;
}

- (void)setDelegate:(id <LineDrawerDelegate>)_delegate {
    delegate = _delegate;
}


@end