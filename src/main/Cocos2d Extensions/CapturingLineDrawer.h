#import "cocos2d.h"
#import "LineDrawer.h"
#import "MarkableGrid.h"


@interface CapturingLineDrawer : CCNode<LineDrawerDelegate> {
    MarkableGrid* mask;
    LineDrawer *drawer;

    CGRect area;
    NSUInteger pointSize;
    id<LineDrawerDelegate> delegate;
}

+ (CapturingLineDrawer *)drawerWithArea:(CGRect)_area pointSize:(NSUInteger)_pointSize strokeSize:(CGFloat)_strokeSize;
- (id)initWithArea:(CGRect)_area pointSize:(NSUInteger)_pointSize strokeSize:(CGFloat)_strokeSize;


-(NSArray *)mask;

- (void)setDelegate:(id <LineDrawerDelegate>)_delegate;

@end