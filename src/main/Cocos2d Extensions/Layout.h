#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface Layout : NSObject

+ (BOOL) isIpad;
+ (CGPoint) iphone:(CGPoint)_iphone ipad:(CGPoint)_ipad;
+ (CGPoint) at:(CGPoint)_point;
+ (CGPoint) center;
+ (CGPoint) top;
+ (CGPoint) `bottom;

@end