#import "cocos2d.h"
#import "SimpleAudioEngine.h"

@interface AnimatedMenu : CCMenu {
	@private CCMenuItem* lastSelectedItem;
	@private CCMenuItem* highlightedItem;
}

-(void) setHighlightedItem:(CCMenuItem*) _selectedItem;

-(void) setEnabled:(BOOL) _enabled;

@end


@interface MenuItemImage : CCMenuItemImage {
}
@end


@interface MenuItemSprite : CCMenuItemSprite {
}
@end


@interface MenuItemFont : CCMenuItemFont {
}
@end

@interface MenuItemToggle : CCMenuItemToggle {
}

@end
