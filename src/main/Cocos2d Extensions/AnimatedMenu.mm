#import "AnimatedMenu.h"

@implementation AnimatedMenu

static const float LK_SIZE_FACTOR = 1.07;
static const float LK_EASE_OUT_PERIOD = 0.3;
static const float LK_SCALE_DURATION = 0.8;

-(CCAction*) createScaleInAction {
	id tmpScale = [CCScaleBy actionWithDuration:LK_SCALE_DURATION scale:LK_SIZE_FACTOR];
	return [CCEaseElasticOut actionWithAction: tmpScale period:LK_EASE_OUT_PERIOD];
}


-(CCAction*) createScaleOutAction {
	id tmpScale = [[CCScaleBy actionWithDuration:LK_SCALE_DURATION scale:LK_SIZE_FACTOR] reverse];
	return [CCEaseElasticOut actionWithAction: tmpScale period:LK_EASE_OUT_PERIOD];
}


-(BOOL) ccTouchBegan:(UITouch *)_touch withEvent:(UIEvent *)_event {
	BOOL result = [super ccTouchBegan:_touch withEvent:_event];

	if (selectedItem_ != nil) {
		[selectedItem_ stopAllActions];
		selectedItem_.scale = 1.0;
		[selectedItem_ runAction:[self createScaleInAction]];
	}
	
	return result;
}


-(void) ccTouchEnded:(UITouch *)_touch withEvent:(UIEvent *)_event {
	if (selectedItem_ != nil) {
		[selectedItem_ stopAllActions];
		selectedItem_.scale = LK_SIZE_FACTOR ;
		[selectedItem_ runAction:[self createScaleOutAction]];
	}
	
	CCMenuItem *currentItem = [self itemForTouch:_touch];
	
	[self setHighlightedItem:highlightedItem];
	
	[super ccTouchEnded:_touch withEvent:_event];
}

-(void) ccTouchMoved:(UITouch *)_touch withEvent:(UIEvent *)_event {
	lastSelectedItem = selectedItem_;
	[super ccTouchMoved:_touch withEvent:_event];
	
	if (selectedItem_ == nil && lastSelectedItem != nil) {
		[lastSelectedItem stopAllActions];
		lastSelectedItem.scale = LK_SIZE_FACTOR;
		[lastSelectedItem runAction:[self createScaleOutAction]];
	}
	
	if (selectedItem_ != nil && selectedItem_ != lastSelectedItem) {
		[lastSelectedItem stopAllActions];
		lastSelectedItem.scale = LK_SIZE_FACTOR;
		[lastSelectedItem runAction:[self createScaleOutAction]];
		
		[selectedItem_ stopAllActions];
		selectedItem_.scale = 1.0;
		[selectedItem_ runAction:[self createScaleInAction]];
	}
}


-(void) setHighlightedItem:(CCMenuItem*) _selectedItem {
	[highlightedItem stopAllActions];
	
	highlightedItem = _selectedItem;
	
	id tmpScaleUp = [CCScaleTo actionWithDuration:0.5 scale:1.1];
	id tmpScaleDown = [CCScaleTo actionWithDuration:0.5 scale:1.0];
	
	id actionSequence = [CCSequence actions: tmpScaleUp, tmpScaleDown, nil];
	
	[highlightedItem runAction:[CCRepeatForever actionWithAction: actionSequence]];
	
	
}

-(void) setEnabled:(BOOL) _enabled {
	for (CCMenuItem* node in self.children) {
		[node setIsEnabled:_enabled];
	}
}

@end



@implementation MenuItemImage

-(void) activate {
	if (isEnabled_) {
        [[SimpleAudioEngine sharedEngine] playEffect:@"menu.wav"];
    }

	[super activate];
}

@end


@implementation MenuItemSprite

-(void) activate {
	if (isEnabled_) {
        [[SimpleAudioEngine sharedEngine] playEffect:@"menu.wav"];
    }

	[super activate];
}

@end



@implementation MenuItemFont

-(void) activate {
	if (isEnabled_) {
        [[SimpleAudioEngine sharedEngine] playEffect:@"menu.wav"];
    }

	[super activate];
}

@end



@implementation MenuItemToggle

-(void) activate {
	if (isEnabled_) {
        [[SimpleAudioEngine sharedEngine] playEffect:@"menu.wav"];
    }

	[super activate];
}

@end
