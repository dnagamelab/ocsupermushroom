#import "GameLayer.h"


@implementation GameLayer {
    BOOL gamePaused;

}

- (void)pauseGame {
    gamePaused = YES;

}

- (void)resumeGame {
    gamePaused = NO;
}

- (BOOL)isGamePaused {
    return gamePaused;
}


@end