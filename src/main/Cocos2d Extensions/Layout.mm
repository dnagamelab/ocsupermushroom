#import "Layout.h"

@implementation Layout
+ (BOOL)isIpad {
    return [CCDirector sharedDirector].winSize.width > 960;
}

+ (CGPoint)at:(CGPoint)_point {
    return ccp([CCDirector sharedDirector].winSize.width * _point.x, [CCDirector sharedDirector].winSize.height * _point.y);
}

+ (CGPoint)center {
    return [Layout at:ccp(0.5, 0.5)];
}

+ (CGPoint)top {
    return [Layout at:ccp(0.5, 1.0)];
}

+ (CGPoint)bottom {
    return [Layout at:ccp(0.5, 0.0)];
}

+ (CGPoint)iphone:(CGPoint)_iphonePoint ipad:(CGPoint)_ipadPoint {
    return [Layout at: ([Layout isIpad] ? _ipadPoint : _iphonePoint) ];
}


@end