#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface GameLayer : CCLayer
- (void)pauseGame;

- (void)resumeGame;

- (BOOL)isGamePaused;
@end