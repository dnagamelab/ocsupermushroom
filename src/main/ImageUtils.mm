#import "ImageUtils.h"
#import "MarkableGrid.h"


@implementation ImageUtils

+ (NSArray *)maskFromImage:(UIImage *)_image withPointSize:(NSUInteger)_pointSize {
    NSUInteger imageWidth = (NSUInteger) _image.size.width;
    NSUInteger imageHeight = (NSUInteger) _image.size.height;

    MarkableGrid * mask = [MarkableGrid gridWithArea:CGSizeMake(imageWidth, imageHeight) fieldSize:_pointSize] ;
    for (NSUInteger currentY = 0; currentY < imageHeight; currentY++) {
        NSArray *array = [ImageUtils pixelsFromImage:_image atX:0 andY:currentY count:imageWidth];

        for (NSUInteger currentX = 0; currentX < imageWidth; currentX++) {
            UIColor *color = [array objectAtIndex:(NSUInteger) currentX];
            CGFloat alphaComponent = CGColorGetComponents([color CGColor])[3];
            if (alphaComponent > 0.1) {
                [mask mark: CGPointMake(currentX, currentY) withValue:[NSNumber numberWithBool:YES]];
            }
        }
    }


    return [mask fields];
}

+ (NSArray *)pixelsFromImage:(UIImage *)_image atX:(NSUInteger)_x andY:(NSUInteger)_y count:(NSUInteger)_count {
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:_count];

    // First get the image into your data buffer
    CGImageRef imageRef = [_image CGImage];
    NSUInteger width = CGImageGetWidth(imageRef);
    NSUInteger height = CGImageGetHeight(imageRef);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    unsigned char *rawData = (unsigned char *) calloc(height * width * 4, sizeof(unsigned char));
    NSUInteger bytesPerPixel = 4;
    NSUInteger bytesPerRow = bytesPerPixel * width;
    NSUInteger bitsPerComponent = 8;
    CGContextRef context = CGBitmapContextCreate(rawData, width, height,
            bitsPerComponent, bytesPerRow, colorSpace,
            kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGColorSpaceRelease(colorSpace);

    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
    CGContextRelease(context);

    // Now your rawData contains the image data in the RGBA8888 pixel format.
    int byteIndex = (bytesPerRow * _y) + _x * bytesPerPixel;
    for (int index = 0; index < _count; ++index) {
        CGFloat red = (rawData[byteIndex] * 1.0) / 255.0;
        CGFloat green = (rawData[byteIndex + 1] * 1.0) / 255.0;
        CGFloat blue = (rawData[byteIndex + 2] * 1.0) / 255.0;
        CGFloat alpha = (rawData[byteIndex + 3] * 1.0) / 255.0;
        byteIndex += 4;

        UIColor *color = [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
        [result addObject:color];
    }

    free(rawData);

    return result;
}

@end
