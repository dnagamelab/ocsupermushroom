#import "Analytics.h"
#import "GANTracker.h"


@implementation Analytics


+ (void)start:(NSString *)_appId {
    NSLog(@"Google Analytics: Starting for %@", _appId);

    [[GANTracker sharedTracker] startTrackerWithAccountID:_appId
                                           dispatchPeriod:10
                                                 delegate:nil];
}

+ (void)stop {
    NSLog(@"Google Analytics: Stopping");
    [[GANTracker sharedTracker] stopTracker];
}

+ (void)trackPageview:(NSString *)_pageview {
    NSLog(@"Google Analytics: Tracking pageview %@", _pageview);

    NSError *error;
    if (![[GANTracker sharedTracker] trackPageview:_pageview
                                         withError:&error]) {
        NSLog(@"ERROR Google Analytics: Tracking pageview %@ (%@)", _pageview, [error description]);
    }
}

+ (void)trackEvent:(NSString *)_category action:(NSString *)_action label:(NSString *)_label {
    NSLog(@"Google Analytics: Track event %@/%@/%@", _category, _action, _label);

    NSError *error;
    if (![[GANTracker sharedTracker] trackEvent:_category
                                         action:_action
                                          label:_label
                                          value:-1
                                      withError:&error]) {
        NSLog(@"ERROR Google Analytics: Track event %@/%@/%@ (%@)", _category, _action, _label, [error description]);
    }
}

+ (void)trackCombined:(NSString *)_category action:(NSString *)_action label:(NSString *)_label {
    [Analytics trackEvent:_category action:_action label:_label];
    [Analytics trackPageview:[NSString stringWithFormat:@"/%@/%@/%@", _category, _action, _label]];
}





@end