#import <Foundation/Foundation.h>


@interface Analytics : NSObject
+ (void)start:(NSString *)_appId;
+ (void)stop;

+ (void)trackPageview:(NSString *)_pageview;
+ (void)trackEvent:(NSString *)_category action:(NSString *)_action label:(NSString *)_label;
+ (void)trackCombined:(NSString *)_category action:(NSString *)_action label:(NSString *)_label;

@end