#import <OCTotallyLazy/OCTotallyLazy.h>
#import "MarkableGrid.h"


@implementation MarkableGrid
@synthesize width;
@synthesize height;

+ (id)gridWithArea:(CGSize)_areaSize fieldSize:(NSUInteger)_fieldSize {
    return [[MarkableGrid alloc] initWithArea:_areaSize fieldSize:_fieldSize];

}

- (id)initWithArea:(CGSize)_areaSize fieldSize:(NSUInteger)_fieldSize {
    if ((self = [super init])) {
        areaSize = _areaSize;
        pointSize = _fieldSize;

        width = (NSUInteger) ceil(areaSize.width / pointSize);
        height = (NSUInteger) ceil(areaSize.height / pointSize);

        size = width * height;
        grid = [NSMutableArray arrayWithCapacity:size];

        for (NSUInteger i = 0; i < size; i++) {
            [grid addObject:[None none]];
        }
    }

    return self;
}

- (void)mark:(CGPoint)_point withValue:(id)_value {
    NSUInteger indexFromPoint = [self gridIndexFromPoint:_point];
    if ([self indexWithinGrid:indexFromPoint]) {
        [grid replaceObjectAtIndex:indexFromPoint
                        withObject:[Some some:_value]];
    }

}

- (void)unmark:(CGPoint)_point {
    NSUInteger indexFromPoint = [self gridIndexFromPoint:_point];
    if ([self indexWithinGrid:indexFromPoint]) {
        [grid replaceObjectAtIndex:indexFromPoint
                        withObject:[None none]];
    }

}

- (NSArray *)fields {
    return [NSArray arrayWithArray:grid];
}

- (Option *)fieldAtPoint:(CGPoint)_point {
    if (![self pointWithinGrid:_point]) {
        @throw [NSException
                exceptionWithName:@"OutOfGridException"
                           reason:[NSString stringWithFormat:@"Point [%f, %f] is outside of the grid", _point.x, _point.y]
                userInfo:nil];
    }

    return [grid objectAtIndex:[self gridIndexFromPoint :_point]];
}

- (BOOL)indexWithinGrid:(NSUInteger)_index {
    return _index > 0 && _index < size;
}

- (BOOL)pointWithinGrid:(CGPoint)_point {
    return [self indexWithinGrid:[self gridIndexFromPoint:_point]];
}


- (NSUInteger)gridIndexFromPoint:(CGPoint)_point {
    NSUInteger positionX = (NSUInteger) (_point.x / pointSize);
    NSUInteger positionY = (((NSUInteger) (_point.y / pointSize)) * width);
    return positionX + positionY;
}


- (NSString *)description {
    NSMutableString *result = [NSMutableString string];
    for (NSUInteger currentY = 0; currentY < height; currentY++) {
        NSMutableString *currentLine = [NSMutableString stringWithCapacity:width];
        for (NSUInteger currentX = 0; currentX < width; currentX++) {
            NSNumber *maskValue = [grid objectAtIndex:currentY * width + currentX];
            [currentLine appendFormat:@"%@", ([maskValue boolValue] ? @"1" : @"0")];
        }


        [result appendFormat:@"%@\n", currentLine];
    }
    return result;
}
@end