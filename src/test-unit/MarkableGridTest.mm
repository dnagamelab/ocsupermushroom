#import <SenTestingKit/SenTestingKit.h>

#define HC_SHORTHAND
#define SM_SHORTHAND

#import <OCHamcrestIOS/OCHamcrestIOS.h>
#import <OCTotallyLazy/OCTotallyLazy.h>
#import <UIKit/UIKit.h>
#import "MarkableGrid.h"
#import "SMShorthands.h"

@interface MarkableGridTest : SenTestCase
@end

@implementation MarkableGridTest

- (void)testGridWithAreaSmallerThanFieldSize {
    MarkableGrid *grid = [MarkableGrid gridWithArea:CGSizeMake(1, 1) fieldSize:10];

    assertThat(num(grid.width), equalTo(num(1)));
    assertThat(num(grid.height), equalTo(num(1)));
}


- (void)testGridWithAreaSameAsFieldSize {
    MarkableGrid *grid = [MarkableGrid gridWithArea:CGSizeMake(10, 10) fieldSize:10];

    assertThat(num(grid.width), equalTo(num(1)));
    assertThat(num(grid.height), equalTo(num(1)));
}


- (void)testGridWithAreaLargerThanFieldSize {
    MarkableGrid *grid = [MarkableGrid gridWithArea:CGSizeMake(11, 11) fieldSize:10];

    assertThat(num(grid.width), equalTo(num(2)));
    assertThat(num(grid.height), equalTo(num(2)));
}


- (void)testGridWithAreaWithDifferentWidthAndHeight {
    MarkableGrid *grid = [MarkableGrid gridWithArea:CGSizeMake(100, 11) fieldSize:10];

    assertThat(num(grid.width), equalTo(num(10)));
    assertThat(num(grid.height), equalTo(num(2)));
}


- (void)testCorrectNumberOfFieldsCreated {
    MarkableGrid *grid = [MarkableGrid gridWithArea:CGSizeMake(100, 11) fieldSize:10];

    assertThat(num([grid.fields count]), equalTo(num(20)));
}


- (void)testFieldsAreNotMarkedInitially {
    MarkableGrid *grid = [MarkableGrid gridWithArea:CGSizeMake(20, 10) fieldSize:10];

    assertThat(num([grid.fields count]), equalTo(num(2)));
    assertThat([[grid fields] objectAtIndex:0], equalTo([None none]));
    assertThat([[grid fields] objectAtIndex:1], equalTo([None none]));
}


- (void)testMarkingTheMiddleOfTheField {
    MarkableGrid *grid = [MarkableGrid gridWithArea:CGSizeMake(100, 100) fieldSize:10];

    [grid mark:CGPointMake(25, 25) withValue:@"marker"];

    assertThat([[grid fields] objectAtIndex:22], equalTo([Some some:@"marker"]));
}


- (void)testMarkingTheEdgeOfTheField {
    MarkableGrid *grid = [MarkableGrid gridWithArea:CGSizeMake(100, 100) fieldSize:10];

    [grid mark:CGPointMake(20, 20) withValue:@"marker"];

    assertThat([[grid fields] objectAtIndex:22], equalTo([Some some:@"marker"]));
}


- (void)testMarkingOutsideOfTheGrid {
    MarkableGrid *grid = [MarkableGrid gridWithArea:CGSizeMake(10, 10) fieldSize:10];

    [grid mark:CGPointMake(100, 100) withValue:@"marker"];

    assertThat([[grid fields] objectAtIndex:0], equalTo([None none]));
}


- (void)testUnmarkingOutsideOfTheGrid {
    MarkableGrid *grid = [MarkableGrid gridWithArea:CGSizeMake(10, 10) fieldSize:10];

    [grid unmark:CGPointMake(100, 100)];

    assertThat([[grid fields] objectAtIndex:0], equalTo([None none]));
}

- (void)testUnmarkingTheField {
    MarkableGrid *grid = [MarkableGrid gridWithArea:CGSizeMake(100, 100) fieldSize:10];
    [grid mark:CGPointMake(25, 25) withValue:@"marker"];

    [grid unmark:CGPointMake(25, 25)];

    assertThat([[grid fields] objectAtIndex:22], equalTo([None none]));
}

- (void)testRetrievingFieldByPoint {
    MarkableGrid *grid = [MarkableGrid gridWithArea:CGSizeMake(100, 100) fieldSize:10];

    [grid mark:CGPointMake(25, 25) withValue:@"marker"];

    assertThat([grid fieldAtPoint:CGPointMake(25, 25)], equalTo([Some some:@"marker"]));
}


- (void)testRetrievingFieldOutsideOfTheGrid {
    MarkableGrid *grid = [MarkableGrid gridWithArea:CGSizeMake(10, 10) fieldSize:10];

    [grid mark:CGPointMake(2, 2) withValue:@"marker"];

    STAssertThrowsSpecificNamed([grid fieldAtPoint:CGPointMake(25, 25)], NSException , @"OutOfGridException", @"Point [2.0, 2.0] is outside of the grid.", nil);
}
@end
