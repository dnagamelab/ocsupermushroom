#import <SenTestingKit/SenTestingKit.h>

#define HC_SHORTHAND
#define SM_SHORTHAND

#import "ImageUtils.h"
#import <OCHamcrestIOS/OCHamcrestIOS.h>
#import <OCTotallyLazy/OCTotallyLazy.h>

@interface ImageUtilsTest : SenTestCase
@end

@implementation ImageUtilsTest

- (void)testExtractingAlphaMaskFromImage {
    UIImage *image = [UIImage imageWithContentsOfFile:[[NSBundle bundleForClass:[ImageUtilsTest class]] pathForResource:@"testPattern" ofType:@"png"]];

    NSString *patternToTest = [self maskToString:[ImageUtils maskFromImage:image withPointSize:1] withWidth:image.size.width];

    assertThat(patternToTest, equalTo(@"| 0000 | 0XX0 | 0XX0 | 0000 |"));
}


- (void)testExtractingPixelsFromImage {
    UIImage *image = [UIImage imageWithContentsOfFile:[[NSBundle bundleForClass:[ImageUtilsTest class]] pathForResource:@"testPattern" ofType:@"png"]];

    NSString *patternToTest = [self maskToString:[ImageUtils pixelsFromImage:image atX:0 andY:0 count:image.size.width * image.size.height] withWidth:image.size.width];

    assertThat(patternToTest, equalTo(@"| (0 0 0 0)(0 0 0 0)(0 0 0 0)(0 0 0 0) | (0 0 0 0)(1 0 0 1)(0 0 0 1)(0 0 0 0) | (0 0 0 0)(0 0 0 1)(0 1 0 1)(0 0 0 0) | (0 0 0 0)(0 0 0 0)(0 0 0 0)(0 0 0 0) |"));
}


- (NSString *)maskToString:(NSArray *)_mask withWidth:(NSUInteger)_width {
    NSMutableString *string = [NSMutableString stringWithCapacity:[_mask count]];
    NSUInteger currentWidth = 0;
    [string appendString:@"| "];
    for (NSObject *item in _mask) {
        currentWidth++;
        if (currentWidth > _width) {
            [string appendString:@" | "];
            currentWidth = 1;
        }

        if ([item isKindOfClass:[None class]]) {
            [string appendString:@"0"];
        }

        if ([item isKindOfClass:[Some class]]) {
            [string appendString:@"X"];
        }

        if ([item isKindOfClass:[UIColor class]]) {
            UIColor* color = (UIColor *) item;

            NSUInteger  red = (NSUInteger) CGColorGetComponents([color CGColor])[0];
            NSUInteger  green = (NSUInteger) CGColorGetComponents([color CGColor])[1];
            NSUInteger  blue = (NSUInteger) CGColorGetComponents([color CGColor])[2];
            NSUInteger alpha = (NSUInteger) CGColorGetComponents([color CGColor])[3];
            [string appendFormat:@"(%d %d %d %d)", red, green, blue, alpha];
        }
    }
    [string appendString:@" |"];

    return string;
}

@end